import React from "react";
import { useQuery, gql } from "@apollo/client";
import Pagination from "@mui/material/Pagination";
import "./App.css";

export const GET_CHARACTERS = gql`
  query Characters($page: Int!) {
    characters(page: $page) {
      info {
        count
        pages
      }
      results {
        id
        name
        status
        species
        type
        gender
        image
        created
      }
    }
  }
`;

function App() {
  const [page, setPage] = React.useState(1);

  const { loading, error, data, refetch } = useQuery(GET_CHARACTERS, {
    variables: { page },
  });

  const handleChange = (event, value) => {
    refetch({ page: value });
    setPage(value);
  };

  const Characters = () => {
    if (loading) return <p>Loading...</p>;

    if (error) return <p>Error : {error.message}</p>;

    return data?.characters?.results.map((item, key) => (
      <div className="character_container" key={key}>
        <div>
          <img src={item?.image} alt="character" />
        </div>

        <div>{item?.name}</div>
      </div>
    ));
  };

  return (
    <div className="App">
      <h1>Rick & Morty characters</h1>

      <Characters />

      <Pagination
        count={data?.characters?.info?.pages}
        page={page}
        onChange={handleChange}
      />
    </div>
  );
}

export default App;
